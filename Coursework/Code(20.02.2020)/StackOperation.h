#ifndef STACK_OPERATION_H
#define STACK_OPERATION_H

#include"Node.h"
#include<math.h>
#include<chrono>

class StackOperation:public Node
{
	public:
		StackOperation();
		~StackOperation();
		//Create an empty node and returns the pointer to the node
		Node* createNode();
		//Whenever you insert the new node make sure you update the last node (This internally should call createNode method)
		Node* insertTop(Node* node,Cards* data);
		//Mix all the elements in the List based on random number mod 52 and swap (Logic :Generate 2 Random Number for each iteration see if they are not same and then swap them)
		void mixDeck(Node* node);
		//Whenever you call withdrawCard, Calculate the last node
		Node* withdrawCard(Node* node);
		//Display all the cards in the Deck
		void displayDeck(Node* node);
	
};	
#endif