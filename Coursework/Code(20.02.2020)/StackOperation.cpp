#include"StackOperation.h"

StackOperation::StackOperation()
{
	
}

StackOperation::~StackOperation()
{
	
}

//Create an empty node and returns the pointer to the node
Node* StackOperation::createNode()
{
	Node* newNode =new Node();
	return(newNode);
}
	
//Whenever you insert the new node make sure you update the last node (Nodehis internally should call createNode method)
Node* StackOperation::insertTop(Node* node,Cards* data)
{
	Node* newNode=createNode();
	newNode->setData(data);
	if(node==NULL)
	{
		node=newNode;
		//node->getData()->showCard();
	}
	else
	{
		Node* tempNode=new Node();
		tempNode=node;
		while(tempNode->getNext()!=NULL)
		{
			tempNode=tempNode->getNext();
			//tempNode->getData()->showCard();
		}
		tempNode->setNext(newNode);
	}
	return(node);
}


//Mix all the elements in the List based on random number mod 52 and swap (Logic :Generate 2 Random Number for each iteration see if they are not same and then swap them)
void StackOperation::mixDeck(Node* node)
{
	int num1;
	int num2;
	int valFlag=0;
	//milliseconds ms = duration_cast< milliseconds >(steady_clock::now().time_since_epoch());
	unsigned __int64 now;
	do
	{
		now = chrono::duration_cast<chrono::milliseconds>(chrono::system_clock::now().time_since_epoch()).count();
	}while((now%1000)<200);
	//cout<<"\nMilliseconds :\t"<<now;
	int shuffleCount=now%1000;
	//cout<<"\nNumber of Mixing :\t"<<shuffleCount;
	for(int mix=1;mix<=shuffleCount;mix++)
	{
		do
		{
			if(valFlag==0)
			{
				num1=rand()%52;
				valFlag=1;
			}
			num2=rand()%52;
		}while(num1==num2);
	
		Node* tempNode=new Node();
		tempNode=node;
		Node* nodeOne=new Node();
		Node* nodeTwo=new Node();
	
		for(int i=1;i<=num1;i++)
			tempNode=tempNode->getNext();
		nodeOne=tempNode;
		tempNode=node;
		for(int i=1;i<=num2;i++)
		{
			tempNode=tempNode->getNext();
		}
		nodeTwo->setData(tempNode->getData());
		tempNode->setData(nodeOne->getData());
		nodeOne->setData(nodeTwo->getData());
		/*
		tempNode->setData(nodeOne->getData());
		nodeOne->setData(nodeTwo->getData());
		nodeTwo->setData(tempNode->getData());
		*/
	}
	
}

//Whenever you call withdrawCard, Calculate the last node
Node* StackOperation::withdrawCard(Node* node)
{
	Node* tempNode=node;
	Node* prevNode;
	for(;tempNode->getNext()->getNext()!=NULL;tempNode=tempNode->getNext())
	{
		prevNode=tempNode;
	}
	tempNode=prevNode;
	prevNode->setNext(NULL);
	return(tempNode->getNext());
}

//Display all the cards in the Deck
void StackOperation::displayDeck(Node* node)
{
	Node* tempNode=new Node();
	tempNode=node;
	while(tempNode!=NULL)
	{
		cout<<"\n";
		tempNode->getData()->showCard();
		tempNode=tempNode->getNext();
	}
}