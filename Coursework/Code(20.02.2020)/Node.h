#ifndef NODE_H
#define NODE_H

#include"Cards.h"

class Node:public Cards
{
	private:
		Cards* data;
		Node* next;
	public:
		Node();
		Node(Cards* data);
		~Node();
		
		//getters and setters 
		Cards* getData();
		void setData(Cards* data);
		Node* getNext();
		void setNext(Node* next);
		
		//Functions
		void printNodeContent();
};
#endif