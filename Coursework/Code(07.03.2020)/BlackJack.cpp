#include"BlackJack.h"

BlackJack::BlackJack()
{
	
}

BlackJack::~BlackJack()
{
	
}

void BlackJack::start()
{
	
		system("CLS");
		CardOperation* cOperation;
	
		//Creating a pack of fair deck of Cards.
		Node* node=cOperation->createDeck();
		//Mixing the deck of Cards
		cOperation->mixDeck(node);
	
		cOperation->displayDeck(node);
	
		vector<Cards*> player1;
		vector<Cards*> computer;
	
		int playerBust=0;
		int computerBust=0;
		
		/*
		//This is to show that after mixing the Deck the card is getting removed from the top
		cout<<"\nAfter Dealing the Deck contains\n";
		cOperation->displayDeck(node);
	
		int i=0;
		cout<<"\nCards in Vectors\n";
		for(Cards* temp:player1)
		{
			++i;
			cout<<"\n"<<i<<" th value for Player 1 is :\t";
			temp->showCard();
		}
		i=0;
		for(Cards* temp:computer)
		{
			++i;
			cout<<"\n"<<i<<" th value for computer is :\t";
			temp->showCard();
		}
		*/
		do
		{
			selectTT(cOperation, node, player1, computer,playerBust, computerBust);
		}while(playerBust!=1||computerBust!=1);
	
	
}

Node* BlackJack::dealCards(Node* node, vector<Cards*>& player, CardOperation* cOperation)
{
	
	Cards* card=new Cards();
	node=cOperation->withdrawTopCard(node,card);
	player.push_back(card);
	
	return(node);
}

int BlackJack::selectTT(CardOperation* cOperation, Node* node, vector<Cards*>& player1, vector<Cards*>& computer, int& playerBust, int& computerBust)
{
	int op;
	do
	{
		//system("CLS");
		cout<<"\n--------------------------------------------------------------------\n";
		cout<<"\n                          select an option   						 \n";
		cout<<"\n--------------------------------------------------------------------\n";
		cout<<"\n1. STICK\n";
		cout<<"\n2. TWIST";
		cout<<"\nYour option is :\t";
		cin>>op;
		if(op<1&&op>2)
		{
			cout<<"\nPlease select an option from the above list\n";
			getch();
		}
		else
		{
			switch(op)
			{
				case 1:
					twist(cOperation,node,computer,2,computerBust);
					break;
				case 2:
					twist(cOperation,node,player1,1,playerBust);
					break;
			}
		}
	}while(op<1&&op>2);
}

/*
int BlackJack::stick(CardOperation* cOperation, Node* node, vector<Cards*>& player, int user)
{
	int threshold=18;
	int points=0;
	int i=2;
	int bust=0;
	cout<<"\nStick coming soon";
	//	node=dealCards(node,player1,cOperation);
	
}
*/

void BlackJack::twist(CardOperation* cOperation, Node* node, vector<Cards*>& player, int user, int& bust)
{
	int winner=1;
	cout<<"\nTwist Coming Soon...";
}

int BlackJack::getPoints(vector<Cards*> cards)
{
	int points=0;
	for(Cards* temp:cards)
	{
		points+=temp->convertBCardFaceToPoints(temp);
	}
	return(points);
}

int BlackJack::checkWinner(int player1,int computer)
{
	int op=1;
	if((player1<=21)&&(player1<computer))
		op=2;
	return(op);
}