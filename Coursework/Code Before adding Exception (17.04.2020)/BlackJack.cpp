#include"BlackJack.h"

BlackJack::BlackJack()
{
	
}

BlackJack::~BlackJack()
{
	
}

void BlackJack::start()
{
	char op;
	int won=0;
	int lost=0;
	int draw=0;
	int round=0;
	//int computerStick=0;
	int playerStick=0;
	ostringstream ss;
	
	ss<<"\n--------------------------------------------------------------------------------\n";
	
	vector<string> numberOfWin;
	vector<GResult> resultVector;
	do
	{
		
		//resetValues(won,draw,lost,playerStick);
		playerStick=0;
		round++;
		ss.clear();
		system("CLS");
		
		CardOperation* cOperation;
	
		//Creating a pack of fair deck of Cards.
		Node* node=cOperation->createDeck();
		
		//Mixing the deck of Cards
		cOperation->mixDeck(node);
	
		cOperation->displayDeck(node);
	
		vector<Cards*> player1;
		vector<Cards*> computer;
	
		int playerBust=0;
		int computerBust=0;
		
		for(int i=0;i<2;i++)
		{
			node=dealCards(node,player1,cOperation);
			node=dealCards(node,computer,cOperation);
		}
		
		/*
		//This is to show that after mixing the Deck the card is getting removed from the top
		cout<<"\nAfter Dealing the Deck contains\n";
		cOperation->displayDeck(node);
	
		int i=0;
		cout<<"\nCards in Vectors\n";
		for(Cards* temp:player1)
		{
			++i;
			cout<<"\n"<<i<<" th value for Player 1 is :\t";
			temp->showCard();
		}
		i=0;
		for(Cards* temp:computer)
		{
			++i;
			cout<<"\n"<<i<<" th value for computer is :\t";
			temp->showCard();
		}
		*/
		
		
		selectTT(cOperation, node, player1, computer, playerBust, computerBust, playerStick);
		//=new GResult(round,"You Won",getPoints(computer),getPoints(player1),"\nHurray!!!\nCongrats...You Won...\n<<<<< That's Brilliant >>>>>");
		if(computerBust==1)
		{
			won++;
			cout<<"\nHurray!!!\nCongrats...You Won...\n<<<<< That's Brilliant >>>>>";
			//ss<<"\nRound :\t"<<round<<"   You Won \t Bank Points :\t"<<getPoints(computer)<<"\t Your Points :\t"<<getPoints(player1);
			//ss<<"\n    "<<round<<"\t    You Won \t       "<<getPoints(computer)<<"   \t\t     "<<getPoints(player1);
			GResult result;
			result.setData(round,"You Won",getPoints(computer),computer.size(),"\nHurray!!!\nCongrats...You Won...\n<<<<< That's Brilliant >>>>>",getPoints(player1),player1.size());
			resultVector.push_back(result);
		}
		if(playerBust==1)
		{
			lost++;
			cout<<"\nBank won....\nBetterluck next Time....\n";
			//ss<<"\nRound :\t"<<round<<"   Bank Won \t Bank Points :\t"<<getPoints(computer)<<"\t Your Points :\t"<<getPoints(player1);
			//ss<<"\n    "<<round<<"\t    Bank Won \t       "<<getPoints(computer)<<"   \t\t     "<<getPoints(player1);
			//result=new Result(round,"Bank Won",getPoints(computer),getPoints(player1),"\nBank won....\nBetterluck next Time....\n");
			GResult result;
			result.setData(round,"Bank Won",getPoints(computer),computer.size(),"\nBank won....\nBetterluck next Time....\n",getPoints(player1),player1.size());
			resultVector.push_back(result);
		}
		if((playerStick==1)&&(getPoints(player1)<getPoints(computer))&&(computerBust!=1))
		{
			lost++;
			cout<<"\nBank won....\nBetterluck next Time....\n";
			//ss<<"\nRound :\t"<<round<<"   Bank Won \t Bank Points :\t"<<getPoints(computer)<<"\t Your Points :\t"<<getPoints(player1);
			//ss<<"\n    "<<round<<"\t    Bank Won \t       "<<getPoints(computer)<<"   \t\t     "<<getPoints(player1);
			//result=new Result(round,"Bank Won",getPoints(computer),getPoints(player1),"\nBank won....\nBetterluck next Time....\n");
			GResult result;
			result.setData(round,"Bank Won",getPoints(computer),computer.size(),"\nBank won....\nBetterluck next Time....\n",getPoints(player1),player1.size());
			resultVector.push_back(result);
		}
		if((playerStick==1)&&(getPoints(player1)==getPoints(computer)))
		{
			draw++;
			cout<<"\nThat's interesting...\nUnfortunately both the players have the same points so, it's a DRAW\nWell Played!!!\n";
			//ss<<"\nRound :\t"<<round<<"   DRAW \t Bank Points :\t"<<getPoints(computer)<<"\t Your Points :\t"<<getPoints(player1);
			//ss<<"\n    "<<round<<"\t   Draw \t       "<<getPoints(computer)<<"   \t\t     "<<getPoints(player1);
			//result=new Result(round,"Bank Won",getPoints(computer),getPoints(player1),"\nThat's interesting...\nUnfortunately both the players have the same points so, it's a DRAW\nWell Played!!!\n");
			GResult result;
			result.setData(round,"Bank Won",getPoints(computer),computer.size(),"\nThat's interesting...\nUnfortunately both the players have the same points so, it's a DRAW\nWell Played!!!\n",getPoints(player1),player1.size());
			resultVector.push_back(result);
		}
		
		cout<<"\nDo you wish to play another game ? (Y/N) :\t";
		cin>>op;		
	}while(op=='Y'||op=='y');
	ostringstream norss;
	norss<<"\nTotal Rounds :\t"<<round;
	ostringstream norw;
	norw<<"\nRounds Won :\t"<<won;
	ostringstream norl;
	norl<<"\nRounds Lost:\t"<<lost;
	ostringstream nord;
	nord<<"\nRounds Draw:\t"<<draw;
	numberOfWin.push_back(norss.str());
	numberOfWin.push_back(norw.str());
	numberOfWin.push_back(norl.str());
	numberOfWin.push_back(nord.str());
	ostringstream headers;
	headers<<"\n\n Rounds Won/Lost Bank Points Cards with Bank  Player Points  Cards with Player\n";
	numberOfWin.push_back(headers.str());
	numberOfWin.push_back(ss.str());
	
	for(GResult temp:resultVector)
	{
		numberOfWin.push_back(temp.displayGResult().str());
	}
	
	gameResults(numberOfWin);
}

Node* BlackJack::dealCards(Node* node, vector<Cards*>& player, CardOperation* cOperation)
{
	Cards* card=new Cards();
	node=cOperation->withdrawTopCard(node,card);
	player.push_back(card);
	return(node);
}

int BlackJack::selectTT(CardOperation* cOperation, Node* node, vector<Cards*>& player1, vector<Cards*>& computer, int& playerBust, int& computerBust, int& playerStick)
{
	int op;
	do
	{
		//system("CLS");
		displayCardsAndScores(player1,1);
		displayCardsAndScores(computer,2);
		
		cout<<"\n\n\n--------------------------------------------------------------------\n";
		cout<<"\n                          select an option   						 \n";
		cout<<"\n--------------------------------------------------------------------\n";
		cout<<"\n1. STICK\n";
		cout<<"\n2. TWIST";
		cout<<"\nYour option is :\t";
		cin>>op;
		if(op<1&&op>2)
		{
			cout<<"\nPlease select an option from the above list\n";
			getch();
		}
		else
		{
			switch(op)
			{
				case 1:
					playerStick=1;
					stick(cOperation,node,computer,computerBust, getPoints(player1));
					//For validation Purpose
					//cout<<"\nPlayer Stick\t"<<playerStick<<"\n";
					//cout<<"\nPlayer Bust\t"<<playerBust<<"\n";
					//cout<<"\nComputer Bust:\t"<<computerBust<<"\n";
					break;
				case 2:
					twist(cOperation,node,player1,playerBust);
					//For validation Purpose
					//cout<<"\nTWIST\tPlayer Stick\t"<<playerStick<<"\n";
					//cout<<"\nTWIST\tPlayer Bust\t"<<playerBust<<"\n";
					//cout<<"\nTWIST\tComputer Stick Bust:\t"<<computerBust<<"\n";
					break;
			}
		}
		//what should be the condition here ?
		if((computerBust==1)||(playerBust==1))
		{
			break;
		}
		else
		{
			if((playerStick==1)&&(getPoints(player1)<getPoints(computer)))
			{
				break;
			}
		}
		//Check if this condition is actually correct
	}while(op>=1&&op<3);
}


void BlackJack::twist(CardOperation* cOperation, Node* node, vector<Cards*>& player, int& bust)
{
	int points=getPoints(player);
	if(points>=21)
	{
		bust=1;
	}
	else
	{
		if(player.size()<5)
		{
			node=dealCards(node,player,cOperation);
			displayCardsAndScores(player,1);
			cout<<"\nLast Card is :\t";
			player.back()->showCard();
			cout<<"\tTotal Points is :\t"<<getPoints(player);
			points=getPoints(player);
			if(points>=21)
				bust=1;
		}
		else
		{
			bust=1;
		}
	}
}

void BlackJack::stick(CardOperation* cOperation, Node* node, vector<Cards*>& player, int& bust, int points)
{
	int playerPoints=0;
	//card should be less than 5
	if(player.size()<5)
	{
		//point is less than 21 (if point is == 21 then it's a bust)
		
		if((getPoints(player)<20)&&(getPoints(player)<=points))
		{
			do
			{
				displayCardsAndScores(player,2);
				node=dealCards(node,player,cOperation);
				cout<<"\nLast Card is :\t";
				player.back()->showCard();
				cout<<"\tTotal Points is :\t"<<getPoints(player);
				playerPoints=getPoints(player);
				if(playerPoints>=21)
				{
					bust=1;
					break;
				}
				else
				{
					//if player points is greater than the points then break
					if(getPoints(player)>points)
					{
						if(getPoints(player)>=21)
							bust=1;
						break;
					}
				}
			}while(true);
		}
	}
}

int BlackJack::getPoints(vector<Cards*> cards)
{
	int points=0;
	for(Cards* temp:cards)
	{
		points+=temp->convertBCardFaceToPoints(temp);
	}
	return(points);
}

void BlackJack::gameResults(vector<string> numberOfWin)
{
	cout<<"\n--------------------------------------------------------------------------------\n\n";
	cout<<"\n                                  Game Results                                     \n";
	cout<<"\n\n--------------------------------------------------------------------------------\n";
	int i=1;
	for(string temp:numberOfWin)
	{
		cout<<temp;
		i++;
	}
}

void BlackJack::displayCardsAndScores(vector<Cards*> player1,int op)
{
		if(op==1)
		{
			cout<<"\n\n\n--------------------------------------------------------------------\n";
			cout<<"\n                    Player has the following cards                      \n";
			cout<<"\n--------------------------------------------------------------------\n";
		}
		else
		{
			cout<<"\n\n\n--------------------------------------------------------------------\n";
			cout<<"\n                      Bank has the following cards                      \n";
			cout<<"\n--------------------------------------------------------------------\n";
		}
		for(Cards* temp:player1)
		{
			cout<<"\n";
			temp->showCard();
		}
		
		cout<<"\n--------------------------------------------------------------------\n";
		cout<<"\n                  Your point is :\t"<<getPoints(player1);
		cout<<"\n--------------------------------------------------------------------\n";
}

void BlackJack::resetValues(int& won,int& draw,int& lost, int& playerStick)
{
	won=0;
	lost=0;
	draw=0;
	playerStick=0;
}