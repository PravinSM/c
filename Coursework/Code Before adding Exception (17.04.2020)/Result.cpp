#include"Result.h"

Result::Result()
{
	
}

Result::Result(int round, string status, int bankPoints, int playerPoints,string description)
{
	this->round=round;
	this->status=status;
	this->bankPoints=bankPoints;
	this->playerPoints=playerPoints;
	this->description=description;
}

Result::~Result()
{

}

void Result::setRounds(int round)
{	
	this->round=round;
}	

int Result::getRounds()
{
	return(round);
}

void Result::setStatus(string status)
{
	this->status=status;
}

string Result::getStatus()
{
	return(status);
}

void Result::setBankPoints(int bankPoints)
{
	this->bankPoints=bankPoints;
}

int Result::getBankPoints()
{
	return(bankPoints);
}

void Result::setPLayerPoints(int playerPoints)
{
	this->playerPoints=playerPoints;
}

int Result::getPlayerPonits()
{
	return(playerPoints);
}

void Result::setDescription(string description)
{
	this->description=description;
}

string Result::getDescription()
{
	return(description);
}

string Result::displayResult()
{
	ostringstream result;
	result<<description<<"\n    "<<round<<"\t    "<<status<<" \t       "<<bankPoints<<"   \t\t     "<<playerPoints;
	return(result.str());
}