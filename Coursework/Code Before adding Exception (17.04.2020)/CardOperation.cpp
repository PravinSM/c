#include"CardOperation.h"

CardOperation::CardOperation()
{
	
}

CardOperation::~CardOperation()
{
	//Validating if Destructor is working fine when we create objects in heap.
	//cout<<"\nCardOperation Destructor call";
}

//Create an empty node and returns the pointer to the node
Node* CardOperation::createNode()
{
	Node* newNode =new Node();
	return(newNode);
}
	
//Whenever you insert the new node make sure you update the last node (Node is internally should call createNode method)
Node* CardOperation::insertTop(Node* node,Cards* data)
{
	Node* newNode=createNode();
	newNode->setData(data);
	if(node==NULL)
	{
		node=newNode;
		//node->getData()->showCard();
	}
	else
	{
		Node* tempNode=new Node();
		tempNode=node;
		while(tempNode->getNext()!=NULL)
		{
			tempNode=tempNode->getNext();
			//tempNode->getData()->showCard();
		}
		tempNode->setNext(newNode);
	}
	return(node);
}

//When ever you withdraw the card make sure you also free the memory space of the corresponding card
Node* CardOperation::withdrawTopCard(Node* node, Cards* topCard)
{
	Node* temp=new Node();
	temp=node;
	Node* lastNode=new Node();
	while(temp->getNext()->getNext()!=NULL)
	{
		temp=temp->getNext();
	}
	lastNode=temp->getNext();
	temp->setNext(NULL);
	topCard->setSuite(lastNode->getData()->getSuite());
	topCard->setFace(lastNode->getData()->getFace());
	delete(lastNode);
	return(node);
}

Node* CardOperation::createDeck()
{
	string f[13]={"TWO","THREE","FOUR","FIVE","SIX","SEVEN","EIGHT","NINE","TEN","JACK","QUEEN","KING","ACE"};
	string s[4]={"SPADE","CLUBS","HEARTS","DIMOND"};
	Node* node=NULL;
	
	for(int i=0;i<4;i++)
	{
		for(int j=0;j<13;j++)
		{
			Cards* card=new Cards(f[j],s[i]);
			node=insertTop(node,card);
		}
	}
	return(node);
}

//Mix all the elements in the List based on random number mod 52 and swap (Logic :Generate 2 Random Number for each iteration see if they are not same and then swap them)
void CardOperation::mixDeck(Node* node)
{
	int num1;
	int num2;
	int valFlag=0;
	//milliseconds ms = duration_cast< milliseconds >(steady_clock::now().time_since_epoch());
	unsigned __int64 now;
	do
	{
		now = chrono::duration_cast<chrono::milliseconds>(chrono::system_clock::now().time_since_epoch()).count();
	}while((now%1000)<200);
	//cout<<"\nMilliseconds :\t"<<now;
	int shuffleCount=now%1000;
	//cout<<"\nNumber of Mixing :\t"<<shuffleCount;
	for(int mix=1;mix<=shuffleCount;mix++)
	{
		do
		{
			if(valFlag==0)
			{
				num1=rand()%52;
				valFlag=1;
			}
			num2=rand()%52;
		}while(num1==num2);
	
		Node* tempNode=new Node();
		tempNode=node;
		Node* nodeOne=new Node();
		Node* nodeTwo=new Node();
	
		for(int i=1;i<=num1;i++)
			tempNode=tempNode->getNext();
		nodeOne=tempNode;
		tempNode=node;
		for(int i=1;i<=num2;i++)
		{
			tempNode=tempNode->getNext();
		}
		nodeTwo->setData(tempNode->getData());
		tempNode->setData(nodeOne->getData());
		nodeOne->setData(nodeTwo->getData());
		/*
		tempNode->setData(nodeOne->getData());
		nodeOne->setData(nodeTwo->getData());
		nodeTwo->setData(tempNode->getData());
		*/
	}
	
}

//Whenever you call withdrawCard, Calculate the last node
Node* CardOperation::withdrawCard(Node* node,Node* prevNode)
{
	Node* tempNode=node;
	
	//Node* prevNode;
	
	for(;tempNode->getNext()->getNext()!=NULL;tempNode=tempNode->getNext());
	
	/*
	{
		prevNode=tempNode;
	}
	tempNode=prevNode;
	*/
	
	prevNode->setData(tempNode->getNext()->getData());
	
	/*
	//The below 3 line of code also works fine, But this implemention is with pointers and we will be able to print the value here
	cout<<"\nThe Picked Card is :\t";
	tempNode->getNext()->getData()->showCard();
	delete(tempNode->getNext());
	*/
	
	delete(tempNode->getNext());
	tempNode->setNext(NULL);
	return(node);
}

Node* CardOperation::pickRandomCard(Node* node)
{
	unsigned __int64 now;
	now = chrono::duration_cast<chrono::milliseconds>(chrono::system_clock::now().time_since_epoch()).count()%52;
	Node* tempNode=node;
	for(int i=1;i<=now;i++)
		tempNode=tempNode->getNext();
	return(tempNode);
}


int CardOperation::compareCards(Cards* cpGuess,Cards* myGuess)
{
	if((cpGuess->getSuite()==myGuess->getSuite())&&(cpGuess->getFace()==myGuess->getFace()))
		return(0);
	else
	{
		
		int cpPoints=convertCardToPoints(cpGuess);
		int myPoints=convertCardToPoints(myGuess);
		//[Validation] - Displaying the function parameter ie.,Card that is picked by Computer
		//cout<<"\ncpGuess Card :\t";
		//cpGuess->showCard();
		//[Validation] - Displaying the points/weight for the card that is picked by computer
		//cout<<"\nCard Operation \t cpPoints :\t"<<cpPoints;
		
		//[Validation] - Displaying the function parameter ie.,Card that is picked by you
		//cout<<"\nmyGuess Card :\t";
		//[Validation] - Displaying the points/weight for the card that you picked
		//myGuess->showCard();
		//cout<<"\nCard Operation \t myPoints :\t"<<myPoints;
		if(cpPoints<myPoints)
			return(1);
		else
			if(cpPoints>myPoints)
				return(-1);
	}
}

int CardOperation::compareFace(Cards* cpGuess,Cards* myGuess)
{
	int cpPoints=convertCardFaceToPoints(cpGuess);
	int myPoints=convertCardFaceToPoints(myGuess);
	if(cpPoints>myPoints)
		return(-1);
	else
		if(cpPoints<myPoints)
			return(1);
		else
			return(0);
}

int CardOperation::compareSuite(Cards* cpGuess,Cards* myGuess)
{
	int cpPoints=convertCardSuiteToPoints(cpGuess);
	int myPoints=convertCardSuiteToPoints(myGuess);
	if(cpPoints>myPoints)
		return(-1);
	else
		if(cpPoints<myPoints)
			return(1);
		else
			return(0);
}

//Display all the cards in the Deck
void CardOperation::displayDeck(Node* node)
{
	Node* tempNode=new Node();
	tempNode=node;
	while(tempNode!=NULL)
	{
		cout<<"\n";
		tempNode->getData()->showCard();
		tempNode=tempNode->getNext();
	}
}