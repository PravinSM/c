#include"Node.h"

Node::Node()
{
	next=NULL;
}

Node::Node(Cards* data)
{
	this->data=data;
	next=NULL;
}

Node::~Node()
{
	//Validating if Destructor is working fine when we create objects in heap.
	//cout<<"\nNode Class Destructor call";
}

Cards* Node::getData()
{
	return(data);
}
	
void Node::setData(Cards* data)
{
	this->data=data;
}

Node* Node::getNext()
{
	return(next);
}

void Node::setNext(Node* next)
{
	this->next=next;
}
	
void Node::printNodeContent()
{
	cout<<"The value of Data is :\t";
	data->showCard();
}
