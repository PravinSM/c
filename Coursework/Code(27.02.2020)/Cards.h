#ifndef CARDS_H
#define CARDS_H

#include<string.h>
#include<iostream>

using namespace std;

class Cards
{
	private:
		string face;
		string suite;
	public:
		Cards();
		Cards(string,string);
		~Cards();
		void setFace(string);
		string getFace();
		void setSuite(string);
		string getSuite();
		
		int convertCardToPoints(Cards*);
		void showCard();
		
};
#endif