#include"CardGuessing.h"

CardGuessing::~CardGuessing()
{
	
}

void CardGuessing::start()
{
	char option;
	do
	{
		system("CLS");
		CardOperation* cOperation;
	
		//Creating a pack of fair deck of Cards.
		Node* node=cOperation->createDeck();
	
		//Mixing the deck of Cards
		cOperation->mixDeck(node);
		//[Validation] - Displaying deck after mixing the cards
		//cOperation->displayDeck(node);
		
		//Picking a random card
		cpCard=cOperation->pickRandomCard(node);
		
		//[Validation] - Displaying the random Card picked by Computer
		//cout<<"\nComputer Picked Card is :\t";
		//cpCard->getData()->showCard();
		
		int validGuessCount=0;
		int compare;
		do
		{
			//Calling the Guess Card Method to make my Selection/Guess from the list of available options
			guessCard();
			
			compare=compareCards(cOperation,validGuessCount);
			
			
		}while(compare!=0);
		
		displayAllValidGuess(validGuessCount);
		
		/*[Validation] - Checking if withdraw function is working properly or not
		Node* cWithdrawn=new Node();
		node=cOperation->withdrawCard(node,cWithdrawn);
		cout<<"\nPicked Card is :\t";
		cWithdrawn->getData()->showCard();
		*/
		
		//checking if user wants to continue with another game
		cout<<"\nDo you wish to play another game ? (Y/N) :\t";
		cin>>option;
	}while(option=='Y'||option=='y');
}

void CardGuessing::guessCard()
{
	string suite=guessSuite();
	string face=guessFace();
	guess=new Node(new Cards(face,suite));
}

string CardGuessing::guessSuite()
{
	int suite;
	do
	{
		system("CLS");
		cout<<"\n-------------------------------------------------------------\n";
		cout<<"\n\n                       Rules                               \n\n";
		cout<<"\n-------------------------------------------------------------\n";
		cout<<"\nYou can Make any possible guess from the Four Suites\n1. SPADE\n2. CLUBS\n3. HEARTS\n4. DIMOND\n\n\nNote : \nThis above order is sorted in Descending \n1. SPADE is bigger compared to all other 3 group\n2. CLUBS is greater than HEARTS and DIMOND\n3. HEARTS is greater than DIMOND\n4. DIMOND is the smallest of them";
		cout<<"\nYour Choice is :\t";
		cin>>suite;
	}while(suite<1||suite>5);
	switch(suite)
	{
		case 1:
			return("SPADE");
		case 2:
			return("CLUBS");
		case 3:
			return("HEARTS");
		case 4:
			return("DIMOND");
	}
}

string CardGuessing::guessFace()
{
	int face;
	do
	{
		system("CLS");
		cout<<"\n-------------------------------------------------------------\n";
		cout<<"\n\n                       Rules                               \n\n";
		cout<<"\n-------------------------------------------------------------\n";
		cout<<"\nYou can Make any possible guess from the 13 Face\n1. ACE\n2. KING\n3. QUEEN\n4. JACK\n5. TEN\n6. NINE\n7. EIGHT\n8. SEVEN\n9. SIX\n10. FIVE\n11. FOUR\n12 THREE\n13 TWO\n\n\nNote : \nThis above order is sorted in Descending \n1. ACE is bigger compared to all other FACES\n2. KING is bigger compared to QUEEN and JACK\n3. QUEEN is bigger compared to JACK\n4. JACK is bigger than rest of the numbers";
		cout<<"\nYour Choice is :\t";
		cin>>face;
	}while(face<1||face>14);
	switch(face)
	{
		case 1:
			return("ACE");
		case 2:
			return("KING");
		case 3:
			return("QUEEN");
		case 4:
			return("JACK");
		case 5:
			return("TEN");
		case 6:
			return("NINE");
		case 7:
			return("EIGHT");
		case 8:
			return("SEVEN");
		case 9:
			return("SIX");
		case 10:
			return("FIVE");
		case 11:
			return("FOUR");
		case 12:
			return("THREE");
		case 13:
			return("TWO");
	}
}

int CardGuessing::compareCards(CardOperation* cOperation,int& validGuessCount)
{
	//[Validation] - Displaying the Card that you picked
	//cout<<"\nCard that you guessed is :\t";
	//guess->getData()->showCard();
	
	int compare=cOperation->compareCards(cpCard->getData(),guess->getData());
	
	//[Validation] - Displaying the output of Compare
	//cout<<"\nCompare :\t"<<compare;
	
	if(compare==0)
	{
		cout<<"\nYou Nailed It...\nHurray...Congrats...You Guessed it Right!!!";
		vGuess[validGuessCount]=new Cards(guess->getData()->getFace(),guess->getData()->getSuite());
	}
	else
		if(cOperation->compareCards(cpCard->getData(),guess->getData())==1)
		{
			cout<<"\nThe Card that you have guessed is ";
			guess->getData()->showCard();
			cout<<" and that's greater\nPlease try again...";
			vGuess[validGuessCount]=new Cards(guess->getData()->getFace(),guess->getData()->getSuite());
		}
		else
		{
			if(cOperation->compareCards(cpCard->getData(),guess->getData())==-1)
			{
				cout<<"\nThe Card that you have guessed is ";
				guess->getData()->showCard();
				cout<<" and that's lesser\nPlease try again...";
				vGuess[validGuessCount]=new Cards(guess->getData()->getFace(),guess->getData()->getSuite());
			}
		}
	validGuessCount++;
	getch();
	return(compare);
}

Node* CardGuessing::getCPCard()
{
	return(cpCard);
}

Node* CardGuessing::getMyGuessedCard()
{
	return(guess);
}

void CardGuessing::displayAllValidGuess(int pos)
{
	
	for(int i=0;i<pos;i++)
	{
		cout<<"\n"<<i+1<<" valid guess is :\t";
		vGuess[i]->showCard();
	}
}