#ifndef STACK_OPERATION_H
#define STACK_OPERATION_H

#include"Node.h"
#include<math.h>
#include<chrono>

class CardOperation:public Node
{
	public:
		CardOperation();
		~CardOperation();
		
		//Create an empty node and returns the pointer to the node
		Node* createNode();
		//Whenever you insert the new node make sure you update the last node (This internally should call createNode method)
		Node* insertTop(Node* node,Cards* data);
		//CreateDeck of Cards in order
		Node* createDeck();
		//Mix all the elements in the List based on random number mod 52 and swap (Logic :Generate 2 Random Number for each iteration see if they are not same and then swap them)
		void mixDeck(Node* node);
		//Whenever you call withdrawCard, Calculate the last node
		Node* withdrawCard(Node*,Node*);
		//Picking a random card
		Node* pickRandomCard(Node*);
		//Display all the cards in the Deck
		void displayDeck(Node*);
		//Get 2 cards as inputs and compares them and gives an integer as output ie., 0 -> EQUAL ; -1 -> LESS ; 1 -> GREATER
		int compareCards(Cards*,Cards*);
	
};	
#endif