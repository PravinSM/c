#include"MyUi.h"

int MyUi::selectGame()
{
	int selectGame;
	do
	{
		system("CLS");
		cout<<"\n--------------------------------------------------------------------\n";
		cout<<"\n                           GAMES                                    \n";
		cout<<"\n--------------------------------------------------------------------\n";
		cout<<"\n1.\tCard Guessing Game";
		cout<<"\n2.\tBlack Jack Game";
		cout<<"\nSelect Your Game :\t";
		cin>>selectGame;
	}while(selectGame>2||selectGame<0);
	return(selectGame);
}
	
void MyUi::playGame(int selectGame)
{
	switch(selectGame)
	{
		case 1:
			{
				CardGuessing* cardGuessing=new CardGuessing();
				cardGuessing->start();
			}
			break;
		case 2:
			{
				BlackJack* blackJack=new BlackJack();
				blackJack->start();
			}
			break;
	}
}