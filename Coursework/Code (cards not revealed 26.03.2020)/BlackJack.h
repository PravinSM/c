#ifndef BLACK_JACK_H
#define BLACK_JACK_H

#include"CardOperation.h"
#include<conio.h>
#include<vector>
#include<sstream>

class BlackJack
{
	
	/*
	private:
		Node* cpCard;
		Cards* vGuess[300];
		Node* guess;
	*/
	public:
		BlackJack();
		~BlackJack();
		
		//Starts the game and all the functions are called with in this
		void start();
		
		
		//Trade First 2 Cards with Player 1 and Computer
		Node* dealCards(Node* node, vector<Cards*>& card, CardOperation* cOperation);
		//This Function should display options whether we are going to trade or twist and return the option that we have selected
		
		int selectTT(CardOperation* cOperation, Node* node, vector<Cards*>& player,vector<Cards*>& computer,int& playerBust,int& computerBust,int& computerStick, int& playerStick);
		
		//stick (Returns 1 if player 1 is the winner, and 2 if computer has won)
		//int stick(CardOperation* cOperation, Node* node, vector<Cards*>& player, int user);
		
		//twist (Returns 1 if player 1 is the winner, and 2 if computer has won)
		//The last variable user is basically a flag, If it's 1 then we assume it's a user who is gonna select b/w twist and stick 
		//and if it's 2, Then it's the bank/computer who is gonna choose b/w Twist and Stick
		void twist(CardOperation* cOperation, Node* node, vector<Cards*>& player, int user, int& bust, int& stick);
		int getPoints(vector<Cards*> cards);
		void gameResults(vector<string> numberOfWin);
		
};

#endif