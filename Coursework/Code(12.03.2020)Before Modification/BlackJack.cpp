#include"BlackJack.h"

BlackJack::BlackJack()
{
	
}

BlackJack::~BlackJack()
{
	
}

void BlackJack::start()
{
	char op;
	int won=0;
	int lost=0;
	int round=0;
	ostringstream ss;
	ss<<"\n--------------------------------------------------------------------------------\n";
	vector<string> numberOfWin;
	do
	{
		round++;
		ss.clear();
		system("CLS");
		
		CardOperation* cOperation;
	
		//Creating a pack of fair deck of Cards.
		Node* node=cOperation->createDeck();
		//Mixing the deck of Cards
		cOperation->mixDeck(node);
	
		cOperation->displayDeck(node);
	
		vector<Cards*> player1;
		vector<Cards*> computer;
	
		int playerBust=0;
		int computerBust=0;
		
		for(int i=0;i<2;i++)
		{
			node=dealCards(node,player1,cOperation);
			node=dealCards(node,computer,cOperation);
		}
		
		/*
		//This is to show that after mixing the Deck the card is getting removed from the top
		cout<<"\nAfter Dealing the Deck contains\n";
		cOperation->displayDeck(node);
	
		int i=0;
		cout<<"\nCards in Vectors\n";
		for(Cards* temp:player1)
		{
			++i;
			cout<<"\n"<<i<<" th value for Player 1 is :\t";
			temp->showCard();
		}
		i=0;
		for(Cards* temp:computer)
		{
			++i;
			cout<<"\n"<<i<<" th value for computer is :\t";
			temp->showCard();
		}
		*/
		
		do
		{
			selectTT(cOperation, node, player1, computer,playerBust, computerBust);
		}while(playerBust!=1&&computerBust!=1);
		
		if(playerBust==1)
		{
			cout<<"\nBank won....\nBetterluck next Time....\n";
			ss<<"\nRound :\t"<<round<<"   Bank Won \t Bank Points :\t"<<getPoints(computer)<<"\t Your Points :\t"<<getPoints(player1);
			//numberOfWin.push_back(ss.str());
			lost++;
		}
		else
		{
			cout<<"\nHurray!!!\nCongrats...You Won...\n<<<<< That's Brilliant >>>>>";
			ss<<"\nRound :\t"<<round<<"   You Won \t Bank Points :\t"<<getPoints(computer)<<"\t Your Points :\t"<<getPoints(player1);
			//numberOfWin.push_back(ss.str());
			won++;
		}
		cout<<"\nDo you wish to play another game ? (Y/N) :\t";
		cin>>op;
		
	}while(op=='Y'||op=='y');
	ostringstream norss;
	norss<<"\nTotal Rounds :\t"<<round;
	ostringstream norw;
	norw<<"\nRounds Won :\t"<<won;
	ostringstream norl;
	norl<<"\nRounds Lost:\t"<<lost;
	numberOfWin.push_back(norss.str());
	numberOfWin.push_back(norw.str());
	numberOfWin.push_back(norl.str());
	numberOfWin.push_back(ss.str());
	
	gameResults(numberOfWin);
}

Node* BlackJack::dealCards(Node* node, vector<Cards*>& player, CardOperation* cOperation)
{
	
	Cards* card=new Cards();
	node=cOperation->withdrawTopCard(node,card);
	player.push_back(card);
	
	return(node);
}

int BlackJack::selectTT(CardOperation* cOperation, Node* node, vector<Cards*>& player1, vector<Cards*>& computer, int& playerBust, int& computerBust)
{
	int op;
	do
	{
		//system("CLS");
		cout<<"\nyou have the following cards :\n\n";
		for(Cards* temp:player1)
		{
			cout<<"\n";
			temp->showCard();
		}
		
		cout<<"\nYour point is :\t"<<getPoints(player1);
		cout<<"\n\n\n--------------------------------------------------------------------\n";
		cout<<"\n                          select an option   						 \n";
		cout<<"\n--------------------------------------------------------------------\n";
		cout<<"\n1. STICK\n";
		cout<<"\n2. TWIST";
		cout<<"\nYour option is :\t";
		cin>>op;
		if(op<1&&op>2)
		{
			cout<<"\nPlease select an option from the above list\n";
			getch();
		}
		else
		{
			switch(op)
			{
				case 1:
					twist(cOperation,node,computer,2,computerBust);
					break;
				case 2:
					twist(cOperation,node,player1,1,playerBust);
					break;
			}
		}
		cout<<"\nPlayer Bust Value :\t"<<playerBust;
		cout<<"\nComputer Bust Value :\t"<<computerBust;
		/*
		if((playerBust==1)||(computerBust==1))
			break;
		&&((playerBust==1)&&(computerBust==1))
		*/
	}while(op<1||op>2);
}

void BlackJack::twist(CardOperation* cOperation, Node* node, vector<Cards*>& player, int user, int& bust)
{
	int points=getPoints(player);
	if(points>=21)
		bust=1;
	else
	{
		node=dealCards(node,player,cOperation);
		points=getPoints(player);
		if(points>=21)
			bust=1;
	}
	
	//cout<<"\nTwist Coming Soon...";
}

int BlackJack::getPoints(vector<Cards*> cards)
{
	int points=0;
	for(Cards* temp:cards)
	{
		points+=temp->convertBCardFaceToPoints(temp);
	}
	return(points);
}

int BlackJack::checkWinner(int player1,int computer)
{
	int op=1;
	if((player1<=21)&&(player1<computer))
		op=2;
	return(op);
}

void BlackJack::gameResults(vector<string> numberOfWin)
{
	cout<<"\n--------------------------------------------------------------------------------\n\n";
	cout<<"\n                                  Game Results                                     \n";
	cout<<"\n\n--------------------------------------------------------------------------------\n";
	int i=1;
	for(string temp:numberOfWin)
	{
		cout<<temp;
		i++;
	}
}