#include"MyUi.h"

int MyUi::selectGame()
{
	int selectGame;
	do
	{
		system("CLS");
		//cout<<"\n--------------------------------------------------------------------\n";
		cout<<"\n ______________________________________________________________";
		cout<<"\n|                           GAMES                              |";
		cout<<"\n|______________________________________________________________|";
		cout<<"\n|                                                              |";
		cout<<"\n|                                                              |";
		cout<<"\n|\t1.Card Guessing Game                                   |";
		cout<<"\n|                                                              |";
		cout<<"\n|\t\t2.Black Jack Game                              |";
		cout<<"\n|                                                              |";
		cout<<"\n|\t\t\t3.Exit                                 |";
		cout<<"\n|______________________________________________________________|";
		cout<<"\n\nSelect Your Game :\t";
		cout<<flush;
		selectGame=validateStringInput();
		if(selectGame==3)
		{
			cout<<"\nThanks for your time...\nHope you had fun playing the game...\nSee you soon!\n\n";
			break;
		}
	}while(selectGame<=0||selectGame>=4);
	return(selectGame);
}
	
void MyUi::playGame(int selectGame)
{
	switch(selectGame)
	{
		case 1:
			{
				CardGuessing* cardGuessing=new CardGuessing();
				cardGuessing->start();
			}
			break;
		case 2:
			{
				BlackJack* blackJack=new BlackJack();
				blackJack->start();
			}
			break;
	}
}

int MyUi::validateStringInput()
{
	int num;
	string sNumber;
	cout<<flush;
	try
	{
		//cout<<"\nYour Input is :\t";
		//cin.ignore();
		getline(cin,sNumber);
		num=stoi(sNumber);
	}
	catch(invalid_argument &m)
	{
		cout<<"\nPlease enter a valid data <Only Number> [Spaces and special characters and strings not allowed]\n";
	}
	return(num);
}