#ifndef CARD_GUESSING_H
#define CARD_GUESSING_H

#include"CardOperation.h"
#include<conio.h>

class CardGuessing
{
	private:
		Node* cpCard;
		Cards* vGuess[300];
		Node* guess;
	public:
	
		~CardGuessing();
	
		Node* getCPCard();
		Node* getMyGuessedCard();
		
		void start();
		void guessCard();
		string guessSuite();
		string guessFace();
		int compareCards(CardOperation*,int&);
		void displayAllValidGuess(int);
		//string rules(int&);
		//void displayAllValidGuess();
		
		int validateStringInput();
		
	
};

#endif